#include "SmartDevice.h"

#define VERSION                         "1.0.2"
#define RELAY_PIN                       16
#define LED_PIN                         14
#define BUTTON_PIN                      13
#define CONFIG_FILE_NAME                "/config.json"
#define EVENTS_CHECK_INTERVAL           60000

#define STATUS_CHECK_INTERVAL           250


SmartDevice smartDevice;

int previousMillisScheduleEventsCheck = millis();
int previousMillisStatusCheck = millis();
bool shouldSaveConfig = false;
int lastStatus = -1;

void notFound(AsyncWebServerRequest * request) {
  request->send(404, "text/plain", "Not found");
}

void configureWiFiManager()
{
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  if (!wifiManager.autoConnect())
  {
    delay(3000);
    ESP.reset();
  }
}

void saveConfigCallback() {
  shouldSaveConfig = true;
}

void readConfiguration() {
  char chipID[32];
  sprintf(chipID, "%08X", ESP.getChipId());
  smartDevice.id = chipID;
  readSPIFFSConfiguration();
}

void readSPIFFSConfiguration()
{
  if (SPIFFS.begin())
  {
    if (SPIFFS.exists(CONFIG_FILE_NAME))
    {

      File configFile = SPIFFS.open(CONFIG_FILE_NAME, "r");
      if (configFile)
      {

        size_t size = configFile.size();
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        StaticJsonBuffer<2048> *jsonBuffer = new StaticJsonBuffer<2048>();
        JsonObject &json = jsonBuffer->parseObject(buf.get());
        if (json.success())
        {

          if (json.containsKey("status"))
          {
            smartDevice.status = json.get<String>("status");
          }
          if (json.containsKey("events"))
          {
            smartDevice.events = json.get<String>("events");
          }
          if (json.containsKey("name"))
          {
            smartDevice.name = json.get<String>("name");
          }
        }
        delete(jsonBuffer);
        configFile.close();
      }
    }
  }
}

void writeDeviceConfiguration()
{
  if (SPIFFS.begin())
  {
    if (SPIFFS.exists(CONFIG_FILE_NAME))
    {
      File configFile = SPIFFS.open(CONFIG_FILE_NAME, "w");
      if (configFile)
      {
        StaticJsonBuffer<2048> *jsonBuffer = new StaticJsonBuffer<2048>();
        JsonObject &doc = jsonBuffer->createObject();
        doc["status"] = smartDevice.status;
        doc["events"] = smartDevice.events;
        doc["name"] = smartDevice.name;

        doc.printTo(configFile);
        configFile.close();
        delete(jsonBuffer);
      }
    }
  }
}

void setupPins()
{
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, smartDevice.status == "on" ? HIGH : LOW);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, smartDevice.status == "on" ? HIGH : LOW);
  pinMode(BUTTON_PIN, INPUT);
}

void setupServer() {
  server.on("/on", HTTP_GET, [](AsyncWebServerRequest * request) {
    smartDevice.status = "on";
    digitalWrite(RELAY_PIN, HIGH);
    digitalWrite(LED_PIN, HIGH);

    request->send(200, "application/json", "{\"status\": \"" + smartDevice.status + "\"}");
    writeDeviceConfiguration();
  });

  server.on("/off", HTTP_GET, [](AsyncWebServerRequest * request) {
    smartDevice.status = "off";
    digitalWrite(RELAY_PIN, LOW);
    digitalWrite(LED_PIN, LOW);

    request->send(200, "application/json", "{\"status\": \"" + smartDevice.status + "\"}");
    writeDeviceConfiguration();
  });


  server.on("/remove-events", HTTP_GET, [](AsyncWebServerRequest * request) {
    smartDevice.events = "[]";
    request->send(200, "application/json", "{\"status\": \"" + smartDevice.status + "\"}");
    writeDeviceConfiguration();
  });

  server.on("/event", HTTP_POST, [](AsyncWebServerRequest * request) {
    if (!request->hasParam("timestamp", true) || !request->hasParam("status", true) || !request->hasParam("days", true) ) {
      request->send(400);
      return;
    }

    StaticJsonBuffer<2048> *jsonBuffer = new StaticJsonBuffer<2048>();
    JsonObject &doc = jsonBuffer->createObject();
    doc["status"] = request->getParam("status", true)->value();
    doc["days"] = request->getParam("days", true)->value();
    doc["timestamp"] = request->getParam("timestamp", true)->value();

    doc.printTo(Serial);

    StaticJsonBuffer<2048> *jsonBuffer1 = new StaticJsonBuffer<2048>();
    JsonArray &root = jsonBuffer1->parseArray(smartDevice.events);
    root.add(doc);

    String s;
    root.printTo(s);
    smartDevice.events = s;

    request->send(200, "application/json", "{\"status\": \"" + smartDevice.status + "\", \"events\": " + smartDevice.events + "}");
    delete(jsonBuffer);
    delete(jsonBuffer1);
  });


  server.on("/status", HTTP_GET, [] (AsyncWebServerRequest * request) {
    request->send(200, "application/json", "{\"status\": \"" + smartDevice.status + "\", \"events\": " + smartDevice.events + ", \"id\": \"" + smartDevice.id + "\"}");
  });


  server.onNotFound(notFound);
  server.begin();
}

void checkButton() {
  if ((millis() - previousMillisStatusCheck) > STATUS_CHECK_INTERVAL) {
    previousMillisStatusCheck = millis();
    int val = digitalRead(BUTTON_PIN);

    if (val == HIGH) {
      lastStatus = 1;
      return;
    }

    if (lastStatus == val) {
      return;
    }

    lastStatus = val;
    digitalWrite(RELAY_PIN, smartDevice.status == "off" ? HIGH : LOW);
    digitalWrite(LED_PIN, smartDevice.status == "off" ? HIGH : LOW);
    smartDevice.status = smartDevice.status == "off" ? "on" : "off";
    writeDeviceConfiguration();
  }
}

void checkEvents()
{
  if ((millis() - previousMillisScheduleEventsCheck) > EVENTS_CHECK_INTERVAL)
  {
    StaticJsonBuffer<2048> *jsonBuffer = new StaticJsonBuffer<2048>();
    JsonArray &root = jsonBuffer->parseArray(smartDevice.events);
    for (auto &object : root)
    {
      String eventTime = object["timestamp"];
      String eventStatus = object["status"];
      String eventDays = object["days"];
      JsonArray &eventWeekdays = jsonBuffer->parseArray(eventDays);

      struct tm *tmp = getLocalTime();

      for (JsonVariant v : eventWeekdays)
      {
        int weekday = v.as<int>();
        if (weekday == tmp->tm_wday)
        {
          int hour = getSplittedStringValue(eventTime, ':', 0).toInt();
          int min = getSplittedStringValue(eventTime, ':', 1).toInt();

          if (tmp->tm_hour == hour && tmp->tm_min == min)
          {
            if (eventStatus == "off")
            {
              digitalWrite(RELAY_PIN, LOW);
              digitalWrite(LED_PIN, LOW);
              smartDevice.status = "off";
            }
            else
            {
              digitalWrite(RELAY_PIN, HIGH);
              digitalWrite(LED_PIN, HIGH);
              smartDevice.status = "on";
            }
            writeDeviceConfiguration();
          }
        }
      }
    }
    delete(jsonBuffer);
    previousMillisScheduleEventsCheck = millis();
  }
}

String getSplittedStringValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++)
  {
    if (data.charAt(i) == separator || i == maxIndex)
    {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
