#include "Arduino.h"
#include <time.h>
#include <sys/time.h>
#include <coredecls.h>

#define TZ              2       // (utc+) TZ in hours
#define DST_MN          0     // use 60mn for summer time in some countries
#define TZ_MN           ((TZ)*60)
#define TZ_SEC          ((TZ)*3600)
#define DST_SEC         ((DST_MN)*60)
#define NTP_SERVER      "pool.ntp.org"

timeval cbtime;
bool cbtime_set = false;

void setupNTP()
{
  settimeofday_cb(time_is_set);
  configTime(TZ_SEC, DST_SEC, NTP_SERVER);
}

void time_is_set()
{
  gettimeofday(&cbtime, NULL);
  cbtime_set = true;
}

struct tm * getLocalTime() {
  timeval tv;
  gettimeofday(&tv, nullptr);
  time_t now = time(nullptr);
  return localtime(&now);
}
