#include <Arduino.h>
#ifdef ESP32
#include <AsyncTCP.h>
#elif defined(ESP8266)
#include <ESPAsyncTCP.h>
#endif

#include "WiFiManager.h" 

#define WEBSERVER_H

#include "ESPAsyncWebServer.h"
#include "DoubleResetDetector.h"
#include "ArduinoJson.h"
#include "FS.h"

#define DRD_TIMEOUT 10
#define DRD_ADDRESS 0

DoubleResetDetector drd(DRD_TIMEOUT, DRD_ADDRESS);
AsyncWebServer server(2414);
WiFiManager wifiManager;

void setup() {
  Serial.begin(115200);

  if (drd.detectDoubleReset())
  {
    wifiManager.resetSettings();
    ESP.restart();
  }

  configureWiFiManager();
  readConfiguration();

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  setupPins();
  setupNTP();
  setupServer();
}

void loop() {
  drd.loop();
  checkEvents();
  checkButton();
}
