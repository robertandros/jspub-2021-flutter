class Interface {
  final String name;
  final String threeBytes;
  List<Address> addresses;

  Interface({this.name, this.addresses, this.threeBytes});

  bool hasAddress(address) => this.addresses.indexOf(address) != -1;
}

class Address {
  final String interface;
  final String ip;
  final int port;

  Address({this.interface, this.ip, this.port = 2414});
}

class SearchData {
  List<Device> devices;

  SearchData(this.devices);
}

class Device {
  String ip;
  int port;
  String id;
  String status;

  Device({this.status, this.ip, this.port, this.id});
}
