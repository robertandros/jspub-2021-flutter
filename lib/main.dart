import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_first_app/pages/device.dart';
import 'package:my_first_app/pages/playground.dart';
import 'package:my_first_app/pages/search.dart';
import 'package:provider/provider.dart';
import 'state_notifier.dart';

void main() {
  runApp(ChangeNotifierProvider(create: (_) => AppStateNotifier(), child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return Consumer<AppStateNotifier>(
      builder: (context, value, child) => MaterialApp(
        initialRoute: '/',
        routes: {
          '/': (context) => SearchScreen(),
          '/device': (context) => DeviceScreen(),
        },
        theme: ThemeData.dark(),
      ),
    );
  }
}
