import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'state_notifier.dart';
import 'menu.dart';

class Wrapper extends StatefulWidget {
  final Widget title;
  final Widget child;
  final bool hasMenu;

  Wrapper({Key key, this.title, this.child, this.hasMenu = true}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  bool _isMenuOpened = false;

  @override
  Widget build(BuildContext context) {
    final AppStateNotifier stateProvider = Provider.of<AppStateNotifier>(context, listen: false);
    return GestureDetector(
      onHorizontalDragUpdate: (_) => null,
      child: Scaffold(
        drawer: widget.hasMenu
            ? Menu(
                callback: (isOpened) {
                  WidgetsBinding.instance.addPostFrameCallback(
                    (_) {
                      setState(() {
                        _isMenuOpened = isOpened;
                      });
                    },
                  );
                },
              )
            : null,
        appBar: PreferredSize(
          preferredSize: const Size(double.infinity, kToolbarHeight),
          child: AppBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                widget.title,
              ],
            ),
            brightness: _isMenuOpened ? (stateProvider.isDarkMode ? Brightness.dark : Brightness.light) : Brightness.dark,
          ),
        ),
        body: widget.child,
      ),
    );
  }
}
