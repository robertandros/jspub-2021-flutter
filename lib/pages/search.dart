import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/foundation.dart';

import 'package:ping_discover_network/ping_discover_network.dart';
import 'package:provider/provider.dart';

import '../state_notifier.dart';
import '../types.dart';
import '../wrapper.dart';

class SearchScreen extends StatefulWidget {
  SearchScreen({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

/* RefreshIndicator, CircularProgressIndicator */
class _MyHomePageState extends State<SearchScreen> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  bool _loading = false;

  Widget _buildBody() {
    return ListView(
      padding: EdgeInsets.all(8.0),
      physics: const BouncingScrollPhysics(
        parent: AlwaysScrollableScrollPhysics(),
      ),
      children: [
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _buildDevices(context),
            ],
          ),
        ),
      ],
    );
  }

  Future<void> _onRefresh() async {
    final AppStateNotifier stateProvider = Provider.of<AppStateNotifier>(context, listen: false);

    setState(() {
      _loading = true;
    });

    _refreshIndicatorKey.currentState?.show();

    _getDevices().then((devices) {
      setState(() {
        _searchData = SearchData(devices);
        stateProvider.setDevices(devices);
        _loading = false;
      });
    });
  }

  final int _port = 2414;
  SearchData _searchData = SearchData(null);

  initState() {
    super.initState();
    final AppStateNotifier stateProvider = Provider.of<AppStateNotifier>(context, listen: false);

    setState(() {
      _loading = true;
    });

    _getDevices().then((devices) {
      setState(() {
        _searchData = SearchData(devices);
        stateProvider.setDevices(devices);

        _loading = false;
      });
    });
  }

  void _onToggleStatus(device) async {
    AppStateNotifier stateProvider = Provider.of<AppStateNotifier>(context, listen: false);

    final action = device.status == 'off' ? 'on' : 'off';
    var uri = Uri.http('${device.ip}:${device.port}', '/$action');

    final response = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
    });

    if (response.statusCode != 200) throw HttpException('${response.statusCode}');

    var parsed = jsonDecode(response.body);
    stateProvider.updateDeviceById(device.id, {'status': parsed['status']});
  }

  Future _getDevices() async {
    List<Interface> interfaces = [];
    // var networkInterfaces = await NetworkInterface.list(includeLoopback: false, type: InternetAddressType.IPv4);

    // bool found = false;

    // networkInterfaces.forEach((interface) {
    //   interface.addresses.forEach((address) {
    //     var parts = address.address.toString().split('.');
    //     if (address.address.indexOf('192.168.') != -1) {
    //       interfaces.add(Interface(name: interface.name, threeBytes: '${parts[0]}.${parts[1]}.${parts[2]}'));
    //       found = true;
    //     }
    //   });
    // });

    // if (!found && !kReleaseMode) {
    interfaces.add(Interface(name: 'debugging1', threeBytes: '192.168.1'));
    // interfaces.add(Interface(name: 'debugging2', threeBytes: '192.168.0'));
    // }

    List<Future<Device>> promises = interfaces.map((element) {
      Completer<Device> c = Completer();
      Device device;

      NetworkAnalyzer.discover2(
        element.threeBytes,
        _port,
        timeout: Duration(milliseconds: 5000),
      ).listen((NetworkAddress addr) async {
        if (addr.exists) {
          final uri = Uri.http('${addr.ip}:$_port', '/status');

          final response = await http.get(uri, headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
          });

          var parsed = jsonDecode(response.body);

          device = Device(status: parsed['status'], ip: addr.ip, port: _port, id: parsed['id']);
        }
      }).onDone(() {
        c.complete(device);
      });
      return c.future;
    }).toList();

    var promisesResult = await Future.wait(promises);
    var result = promisesResult.where((element) => element != null).toList();
    return result;
  }

  Widget _buildDevices(BuildContext context) {
    if (_loading) {
      return Container();
    }

    if (_searchData.devices == null || _searchData.devices.length == 0) {
      return Container(
        padding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
        height: MediaQuery.of(context).size.height * 0.75,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'No device found!',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
              Container(
                child: null,
                margin: EdgeInsets.symmetric(vertical: 10),
              ),
            ],
          ),
        ),
      );
    }

    AppStateNotifier stateProvider = Provider.of<AppStateNotifier>(context, listen: false);

    return Container(
      width: MediaQuery.of(context).size.width * 0.65,
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            child: Text(
              'Devices',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            padding: EdgeInsets.only(bottom: 10),
          ),
          ListView(
            shrinkWrap: true,
            children: _searchData.devices.asMap().entries.map((f) {
              var index = f.key;
              var device = f.value;

              return Container(
                height: 100,
                padding: EdgeInsets.zero,
                margin: EdgeInsets.symmetric(horizontal: 5),
                decoration: BoxDecoration(
                  border: index != _searchData.devices.length - 1
                      ? null
                      : Border(
                          bottom: BorderSide(
                            color: Theme.of(context).textTheme.bodyText1.color,
                          ),
                        ),
                ),
                child: Center(
                  child: ListTile(
                    onTap: () => Navigator.of(context).pushNamed('/device', arguments: device),
                    leading: Container(
                      child: Icon(
                        Icons.lightbulb,
                        size: 64,
                        color: stateProvider.isDarkMode ? Colors.white : Colors.blueGrey,
                      ),
                    ),
                    title: Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'Device $index',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                    ),
                    subtitle: Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        '${device.ip}:${device.port}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                        ),
                      ),
                    ),
                    trailing: Container(
                      width: 72,
                      height: 72,
                      decoration: BoxDecoration(shape: BoxShape.circle),
                      child: FlatButton(
                        shape: CircleBorder(),
                        onPressed: () => _onToggleStatus(device),
                        child: Image.network('https://static.thenounproject.com/png/294411-200.png',
                            width: 32, height: 32, color: device.status == 'off' ? Colors.red : Colors.lightGreen),
                      ),
                    ),
                  ),
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Wrapper(
      title: Text('Search'),
      child: Container(
        child: _buildBody(),
      ),
    );
  }
}
