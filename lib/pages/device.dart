import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../state_notifier.dart';
import '../types.dart';
import '../wrapper.dart';

class DeviceScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DeviceScreenState();
}

class _DeviceScreenState extends State<DeviceScreen> {
  bool loaded = false;
  String url;
  Device device;

  void _onToggleStatus() async {
    AppStateNotifier stateProvider = Provider.of<AppStateNotifier>(context, listen: false);

    final action = device.status == 'off' ? 'on' : 'off';
    var uri = Uri.http(url, '/$action');

    final response = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
    });

    if (response.statusCode != 200) throw HttpException('${response.statusCode}');

    var parsed = jsonDecode(response.body);
    stateProvider.updateDeviceById(device.id, {'status': parsed['status']});
  }

  Widget _buildDevice() {
    AppStateNotifier stateProvider = Provider.of<AppStateNotifier>(context, listen: false);

    final String status = device.status;
    Color iconBgColor = stateProvider.isDarkMode ? ThemeData.dark().scaffoldBackgroundColor : ThemeData.light().scaffoldBackgroundColor;

    Color iconColor = stateProvider.isDarkMode ? Colors.white : Colors.blueGrey[600];

    return Center(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: Column(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          Container(
            margin: EdgeInsets.only(
              top: 60,
              bottom: 30,
            ),
            child: CircleAvatar(
              backgroundColor: status == 'off' ? Colors.red : Colors.lightGreen,
              radius: 68,
              child: CircleAvatar(
                backgroundColor: iconBgColor,
                radius: 64,
                child: CircleAvatar(
                  backgroundColor: iconBgColor,
                  radius: 60,
                  child: FlatButton(
                    shape: CircleBorder(),
                    height: 160,
                    onPressed: () => _onToggleStatus(),
                    child: Image.network('https://static.thenounproject.com/png/294411-200.png', height: 60, width: 60, color: iconColor),
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Text(
              'and other useful \n features... 😲',
              style: TextStyle(fontSize: 24),
            ),
          )
        ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    device = ModalRoute.of(context).settings.arguments;
    url = '${device.ip}:${device.port}';
    return Wrapper(title: Text('Device'), child: _buildDevice());
  }
}
