import 'package:flutter/material.dart';
import 'package:scratcher/scratcher.dart';

import '../wrapper.dart';

class PlaygroundScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PlaygroundScreenState();
}

class _PlaygroundScreenState extends State<PlaygroundScreen> {
  var _scratchValue = 0.0;
  @override
  Widget build(BuildContext context) {
    return Wrapper(
        child: Container(
          child: Center(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: 30),
                  child: Text(
                    '${_scratchValue.toStringAsFixed(2)}%',
                    style: TextStyle(fontSize: 24),
                  ),
                ),
                Scratcher(
                  brushSize: 30,
                  threshold: 50,
                  color: Colors.red,
                  onChange: (value) => setState(() => _scratchValue = value),
                  child: Container(
                    height: 200,
                    width: 300,
                    child: Image.network(
                        'https://cdn.contentspeed.ro/slir/w1000/serviciipublice.websales.ro/cs-content/cs-photos/blog/original//4c5a96439eb277884a9fc791d665282f_1592218254.jpg'),
                  ),
                )
              ],
            ),
          ),
        ),
        title: Text('Playground'),
        hasMenu: false);
  }
}
