import 'package:flutter/material.dart';
import 'package:my_first_app/types.dart';

class AppStateNotifier extends ChangeNotifier {
  bool isDarkMode = true;

  void updateTheme(bool isDarkMode) {
    this.isDarkMode = isDarkMode;
    notifyListeners();
  }

  List<Device> devices;

  void updateDeviceById(String id, Map<String, dynamic> properties) {
    Device device = devices.where((element) => element.id == id).single;

    if (device == null) {
      return;
    }

    for (var prop in properties.entries) {
      if (prop.key == 'status') {
        device.status = prop.value;
      }
    }

    notifyListeners();
  }

  void setDevices(devices) {
    this.devices = devices;
    notifyListeners();
  }
}
