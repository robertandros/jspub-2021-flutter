import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'state_notifier.dart';

class Menu extends StatefulWidget {
  Menu({
    Key key,
    this.elevation = 16.0,
    this.child,
    this.semanticLabel,
    this.callback,
  })  : assert(elevation != null && elevation >= 0.0),
        super(key: key);

  final double elevation;
  final Widget child;
  final String semanticLabel;

  final DrawerCallback callback;

  @override
  _MenuState createState() => _MenuState();
}

/*  Navigator.of(context).pushNamed('/playground'); */
class _MenuState extends State<Menu> {
  void _updateTheme(bool value, AppStateNotifier stateProvider) {}

  List<Widget> _buildMenu() {
    final AppStateNotifier stateProvider = Provider.of<AppStateNotifier>(context, listen: false);
    final bool isDarkMode = stateProvider.isDarkMode;

    return [
      ListTile(
        title: Text('Dark mode'),
        trailing: Switch(
          value: isDarkMode,
          onChanged: (value) => _updateTheme(value, stateProvider),
        ),
      ),
      ListTile(
        title: Text('Playground'),
        leading: Icon(Icons.warning_amber_rounded),
        onTap: () {},
      )
    ];
  }

  @override
  void initState() {
    if (widget.callback != null) {
      widget.callback(true);
    }

    super.initState();
  }

  @override
  void dispose() {
    if (widget.callback != null) {
      widget.callback(false);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Drawer(
        child: Scaffold(
          body: ListView(
            children: <Widget>[
              Container(
                height: 220,
                child: DrawerHeader(
                  child: Image.network(
                    'https://jspub.ro/assets/images/logo.png',
                    fit: BoxFit.contain,
                  ),
                  decoration: BoxDecoration(),
                  margin: const EdgeInsets.only(top: 10, bottom: 10),
                  padding: EdgeInsets.zero,
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  children: _buildMenu(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
